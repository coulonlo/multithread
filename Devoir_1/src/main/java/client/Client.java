package client;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.Socket;

/**
 * Classe représentant un client qui se connecte à un serveur et gère l'envoi et la réception des messages.
 */
@Slf4j
public class Client {

    private final String host;
    private final int port;
    private final Socket serverSocket;

    /**
     * Constructeur de la classe Client.
     *
     * @param host L'adresse du serveur auquel se connecter.
     * @param port Le port du serveur auquel se connecter.
     * @throws IOException Si une erreur se produit lors de la connexion au serveur.
     */
    public Client(String host, int port) throws IOException {
        this.host = host;
        this.port = port;
        this.serverSocket = new Socket(host, port);
        log.info("Client connecté au serveur {} sur le port {}", host, port);
    }

    /**
     * Lance deux threads :
     * <ol>
     *     <li>Le premier gère la réception des messages du côté client.</li>
     *     <li>Le second gère l'envoi des messages vers le serveur.</li>
     * </ol>
     */
    public void run() {
        try {
            ClientMessageReceptor clientMessageReceptor = new ClientMessageReceptor(serverSocket);
            ClientMessageSender clientMessageSender = new ClientMessageSender(serverSocket);

            clientMessageReceptor.start();
            clientMessageSender.start();

            log.info("Socket client initialisée avec l'hôte : {} et le port : {}", host, port);
        } catch (Exception e) {
            log.error("Erreur lors du démarrage des threads de réception et d'envoi des messages : {}", e.getMessage(), e);
            closeConnection();
        }
    }

    /**
     * Ferme la connexion avec le serveur.
     */
    private void closeConnection() {
        try {
            if (serverSocket != null && !serverSocket.isClosed()) {
                serverSocket.close();
                log.info("Connexion avec le serveur {} fermée.", host);
            }
        } catch (IOException e) {
            log.error("Erreur lors de la fermeture de la connexion avec le serveur : {}", e.getMessage(), e);
        }
    }
}
