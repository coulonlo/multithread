package client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

/**
 * Classe principale pour démarrer l'application client.
 */
@SpringBootApplication
@Slf4j
public class ClientApp2 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ClientApp.class, args);
    }

    /**
     * Méthode exécutée au démarrage de l'application.
     * Elle initialise le client et le connecte au serveur.
     *
     * @param args Arguments de la ligne de commande.
     */
    @Override
    public void run(String... args) {
        log.info("Démarrage du client...");
        try {
            Client client = new Client("localhost", 35000);
            client.run();
        } catch (IOException exception) {
            log.error("Le serveur est probablement hors ligne. " +
                    "Réessayez ou activez le niveau de log debug pour plus d'informations sur l'exception.");
            log.debug("Exception lors de l'initialisation du client :", exception);
        }
    }
}
