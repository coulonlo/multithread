package client;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

/**
 * Cette classe étend la classe Thread de Java et gère la réception des messages du serveur pour ce client.
 */
@Slf4j
@AllArgsConstructor
public class ClientMessageReceptor extends Thread {

    @NonNull
    private final Socket serverSocket;

    /**
     * Méthode principale exécutée par le thread pour recevoir les messages du serveur.
     * Elle les affiche dans la console et se termine proprement lorsque la connexion est fermée.
     */
    @Override
    public void run() {
        try (DataInputStream serverInputStream = new DataInputStream(serverSocket.getInputStream())) {
            while (!serverSocket.isClosed()) {
                String message = serverInputStream.readUTF();
                System.out.println(message);
            }
        } catch (SocketException socketException) {
            handleSocketException(socketException);
        } catch (IOException ioException) {
            log.error("Erreur de réception des messages. Cause : ", ioException);
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Gère les exceptions liées au socket, comme la fermeture inattendue de la connexion.
     *
     * @param socketException l'exception de type SocketException à gérer.
     */
    private void handleSocketException(SocketException socketException) {
        log.debug("Flux d'entrée fermé.");
        if (!serverSocket.isClosed()) {
            log.error("Le serveur est probablement hors ligne. Terminez l'application. Activez le mode débogage pour plus de détails.");
            log.debug("Exception détaillée : ", socketException);
            System.exit(1);
        }
    }
}
