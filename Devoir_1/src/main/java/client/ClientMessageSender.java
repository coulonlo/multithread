package client;

import lombok.extern.slf4j.Slf4j;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.Objects;
import java.util.Scanner;

/**
 * Cette classe étend la classe Thread de Java et gère l'envoi des messages du client au serveur.
 */
@Slf4j
public class ClientMessageSender extends Thread {

    private final Socket serverSocket;

    /**
     * Constructeur de la classe ClientMessageSender.
     *
     * @param serverSocket Le socket utilisé pour envoyer des messages au serveur.
     */
    public ClientMessageSender(Socket serverSocket) {
        this.serverSocket = serverSocket;
    }

    /**
     * Méthode principale exécutée par le thread.
     * Gère l'envoi des messages du client au serveur, et ferme la connexion lorsque le message 'exit' est envoyé.
     */
    @Override
    public void run() {
        try (Scanner scanner = new Scanner(System.in);
             DataOutputStream serverOutputStream = new DataOutputStream(serverSocket.getOutputStream())) {

            log.info("Prêt à envoyer des messages au serveur. Tapez 'exit' pour quitter.");

            while (true) {
                String input = scanner.nextLine();
                serverOutputStream.writeUTF(input);

                if (Objects.equals(input.trim(), "exit")) {
                    break;
                }
            }

            closeConnection();

        } catch (SocketException socketException) {
            handleSocketException(socketException);
        } catch (IOException ioException) {
            log.error("Erreur lors de l'envoi du message. Cause : ", ioException);
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Gère les exceptions liées au socket, comme la fermeture inattendue de la connexion.
     *
     * @param socketException l'exception de type SocketException à gérer.
     */
    private void handleSocketException(SocketException socketException) {
        log.error("Le serveur est probablement hors ligne. Terminez l'application. Activez le mode débogage pour plus de détails.");
        log.debug("Exception détaillée : ", socketException);
    }

    /**
     * Ferme la connexion avec le serveur proprement.
     */
    private void closeConnection() {
        try {
            if (serverSocket != null && !serverSocket.isClosed()) {
                serverSocket.close();
                log.info("Connexion avec le serveur fermée.");
            }
        } catch (IOException e) {
            log.error("Erreur lors de la fermeture de la connexion avec le serveur : ", e);
        }
    }
}
