package serveur;

import lombok.extern.slf4j.Slf4j;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentMap;

/**
 * Cette classe étend la classe Thread de Java et gère la réception et la diffusion des messages d'un seul client.
 * Elle communique avec le client via son attribut Socket.
 */
@Slf4j
public class MessageReceptor extends Thread {

    private final Socket client;
    private final DataInputStream clientInputStream;
    private final ConcurrentMap<Socket, String> clientsMap;
    private String pseudo;

    /**
     * Constructeur de la classe MessageReceptor.
     * Initialise l'attribut clientInputStream nécessaire pour recevoir les données du client.
     *
     * @param client     Le socket représentant le client.
     * @param clientsMap La carte concurrente des clients connectés et leurs pseudonymes.
     */
    public MessageReceptor(Socket client, ConcurrentMap<Socket, String> clientsMap) {
        this.client = client;
        this.clientsMap = clientsMap;
        try {
            this.clientInputStream = new DataInputStream(client.getInputStream());
        } catch (IOException exception) {
            throw new MessageReceptorRuntimeException("Impossible de créer le thread MessageReceptor.", exception);
        }
    }

    /**
     * Méthode principale exécutée par le thread. Elle gère l'initialisation de l'utilisateur,
     * puis écoute continuellement les messages envoyés par le client pour les diffuser ou le déconnecter.
     */
    @Override
    public void run() {
        initUser();
        try {
            while (true) {
                String inputMessage = clientInputStream.readUTF();
                log.debug("Nouveau message de {}: {}", pseudo, inputMessage);

                if ("exit".equalsIgnoreCase(inputMessage.trim())) {
                    disconnectClient();
                    break;
                }

                broadcastMessage(pseudo, inputMessage);
            }
        } catch (IOException exception) {
            log.warn("{} s'est déconnecté sans utiliser la commande 'exit'.", pseudo);
            log.debug("Erreur : ", exception);
            try {
                disconnectClient();
            } catch (IOException ioException) {
                log.error("Impossible de déconnecter l'utilisateur {}", pseudo, ioException);
            }
        }
    }

    /**
     * Diffuse un message indiquant qu'un nouvel utilisateur a rejoint la conversation.
     */
    private void broadcastConnection() throws IOException {
        for (Map.Entry<Socket, String> otherClient : clientsMap.entrySet()) {
            if (isClientIdentified(otherClient) && !otherClient.getKey().equals(client)) {
                sendMessageToClient(otherClient.getKey(), pseudo + " a rejoint la conversation.");
            }
        }
    }

    /**
     * Déconnecte le client et notifie tous les autres clients.
     */
    private void disconnectClient() throws IOException {
        clientsMap.remove(client);
        log.info("{} a quitté la conversation.", pseudo);
        for (Map.Entry<Socket, String> otherClient : clientsMap.entrySet()) {
            sendMessageToClient(otherClient.getKey(), pseudo + " a quitté la conversation.");
        }
        client.close();
    }

    /**
     * Diffuse un message reçu d'un client à tous les autres clients.
     */
    private void broadcastMessage(String pseudo, String inputMessage) {
        for (Map.Entry<Socket, String> otherClient : clientsMap.entrySet()) {
            if (!isClientIdentified(otherClient) || otherClient.getKey().equals(client)) continue;
            sendMessageToClient(otherClient.getKey(), pseudo + " a dit : " + inputMessage);
        }
    }

    /**
     * Initialise le pseudo de l'utilisateur, vérifie son unicité et notifie tous les autres clients de sa connexion.
     */
    private void initUser() {
        try {
            DataOutputStream clientOutputStream = new DataOutputStream(client.getOutputStream());
            clientOutputStream.writeUTF("Entrez votre pseudo : ");
            pseudo = clientInputStream.readUTF().trim();

            while (pseudo.isEmpty() || clientsMap.containsValue(pseudo)) {
                clientOutputStream.writeUTF("Pseudo déjà utilisé ou invalide. Entrez un nouveau pseudo : ");
                pseudo = clientInputStream.readUTF().trim();
            }

            clientsMap.put(client, pseudo);
            printConnectedClients(clientOutputStream);
            broadcastConnection();
            log.info("{} a rejoint la conversation.", pseudo);
            clientOutputStream.writeUTF("Connexion réussie ! Bienvenue " + pseudo);
        } catch (IOException exception) {
            log.warn("Impossible de connecter le client entrant.");
            log.debug("Exception : ", exception);
        }
    }

    /**
     * Envoie la liste des utilisateurs connectés à l'utilisateur actuel.
     */
    private void printConnectedClients(DataOutputStream clientOutputStream) {
        try {
            for (Map.Entry<Socket, String> otherClient : clientsMap.entrySet()) {
                if (isClientIdentified(otherClient) && !otherClient.getKey().equals(client)) {
                    clientOutputStream.writeUTF(otherClient.getValue() + " est dans la conversation.");
                }
            }
        } catch (IOException exception) {
            log.error("Impossible d'afficher les utilisateurs connectés.", exception);
        }
    }

    /**
     * Vérifie si un client est identifié par un pseudo valide.
     */
    private static boolean isClientIdentified(Map.Entry<Socket, String> client) {
        return client.getValue() != null && !client.getValue().isEmpty();
    }

    /**
     * Envoie un message à un client spécifique.
     */
    private void sendMessageToClient(Socket clientSocket, String message) {
        try {
            DataOutputStream clientOutputStream = new DataOutputStream(clientSocket.getOutputStream());
            clientOutputStream.writeUTF(message);
        } catch (IOException exception) {
            log.error("Impossible d'envoyer le message au client : {}", message, exception);
        }
    }
}
