package serveur;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Slf4j
public class Server {

    private final int port;
    private final ServerSocket serverSocket;
    private final ConcurrentMap<Socket, String> clients;

    /**
     * Constructeur de la classe Server.
     *
     * @param port Le port sur lequel le serveur écoute les connexions entrantes.
     * @throws IOException Si une erreur se produit lors de l'initialisation du ServerSocket.
     */
    public Server(int port) throws IOException {
        this.port = port;
        this.serverSocket = new ServerSocket(this.port);
        this.clients = new ConcurrentHashMap<>();
        log.info("Serveur créé sur le port {}", port);
    }

    /**
     * Démarre le serveur pour accepter les connexions des clients.
     * Attend continuellement les connexions entrantes et démarre un nouveau thread pour gérer chaque client.
     */
    public void run() {
        log.info("Serveur démarré et en attente de connexions sur le port {}", port);

        while (true) {
            try {
                Socket clientSocket = serverSocket.accept();
                log.info("Nouvelle connexion client : {}", clientSocket.getRemoteSocketAddress());
                clients.put(clientSocket, "");

                // Démarrage d'un nouveau thread pour gérer la communication avec le client
                MessageReceptor clientMessageReceptor = new MessageReceptor(clientSocket, clients);
                clientMessageReceptor.start();
            } catch (IOException e) {
                log.error("Échec de la connexion client : {}", e.getMessage(), e);
            }
        }
    }
}
