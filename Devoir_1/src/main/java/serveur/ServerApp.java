package serveur;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class ServerApp implements CommandLineRunner {

    private static final int SERVER_PORT = 35000; // Constante pour le port du serveur

    public static void main(String[] args) {
        SpringApplication.run(ServerApp.class, args);
    }

    /**
     * Méthode exécutée au démarrage de l'application.
     *
     * @param args Arguments de la ligne de commande
     */
    @Override
    public void run(String... args) {
        try {
            log.info("Démarrage du serveur...");
            Server server = new Server(SERVER_PORT);
            server.run();
            log.info("Serveur démarré sur le port {}", SERVER_PORT);
        } catch (Exception e) {
            log.error("Erreur lors du démarrage du serveur : {}", e.getMessage());
            throw new RuntimeException("Le serveur n'a pas pu démarrer.", e);
        }
    }
}
