package serveur;

/**
 * Exception dédiée aux erreurs non critiques dans la classe MessageReceptor.
 * Elle est utilisée pour encapsuler les exceptions liées à la gestion des messages des clients.
 */
public class MessageReceptorRuntimeException extends RuntimeException {

    /**
     * Constructeur de l'exception.
     *
     * @param message   Le message décrivant l'erreur.
     * @param cause     L'exception d'origine à l'origine de cette erreur.
     */
    public MessageReceptorRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
